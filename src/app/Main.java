package app;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        File file = new File("test.txt");
        List<User> users = new ArrayList<>();

        try {
            users = createAndAddUser(file, users);
        } catch (IOException e) {
            e.printStackTrace();
        }

        users.forEach(System.out::println);

    }

    static List<User> createAndAddUser(File file, List<User> collection) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String str;
            while ((str = reader.readLine()) != null) {
                User user = new User(str, reader.readLine(), Integer.parseInt(reader.readLine()));
                collection.add(user);
            }
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
        return collection;
    }
}
